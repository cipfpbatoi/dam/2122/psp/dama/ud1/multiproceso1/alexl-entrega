import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;



public class Ejercicio1 {
    public static void main(String[] args) {



        try {
            File file = new File("output.txt");
            //Como escribir el comando "cmd /c dir";


            List<String> lista = Arrays.asList(args);
            FileOutputStream fos=new FileOutputStream(file,false);
            DataOutputStream dos=new DataOutputStream(fos);


            Process process = new ProcessBuilder(lista).start();

            // usando los comandos


// ProcessBuilder.directory(new File("ruta")); donde ruta = la carpeta del ejecutable

            // Se lee la salida
            InputStream is = process.getInputStream();

            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            //Si el proceso hijo tarda mas de 2 segundos hace una cosa o ota
            if (!process.waitFor(2,TimeUnit.SECONDS)) {
                //muestra el mensaje  y acaba el programa
                System.out.println("El proceso del hijo tarda demasiado");
                System.exit(0);
            } else {
                /**
                 * No se comprueba si el proceso ha finalizado correctamente.
                 * Es el valor de retorno del proceso.
                 */
                //lee la line
                a del buffer y la almacena en linea
                String line;
                //mientras que la linea sea distinta a null hace el while
                while ((line = br.readLine()) != null) {
                    //muestra la linea y la guarda en el documento
                    System.out.println(line);
                    dos.writeUTF(line);

                    /**
                     * writeUTF no se debe utilizar.
                     * No se cierra el archivo, por tanto, no se guardaria.
                     */
                }
            }
        }catch (IOException | InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}

