import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Ejercicio2 {
    public static void main(String[] args) {
        //nos almacena en la veriable el nombre del usuario
        String nombre=System.getProperty("user.name");
        //ponemos la ruta donde se encuentra nuestro .jar
        String comando="java -jar C:\\Users\\"+nombre+"\\Desktop\\ejercicioarturo\\Ejercicio2\\out\\artifacts\\Ejercicio1_jar\\Ejercicio1.jar";
        //El fichero donde se guarda la informacion
        File file=new File("randoms.txt");
        //pasamos los argumentos a una lista de tipo string
        List<String> lineas= Arrays.asList(comando.split(" "));
        //inicializamos el ProcessBuilder
        ProcessBuilder pb= new ProcessBuilder(lineas);
        //
        pb.inheritIO();
        //
        pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);

        try {
            pb.redirectOutput(ProcessBuilder.Redirect.to(file));
            Process process=pb.start();
            //hace que el proceso se espere
            process.waitFor();

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }


    }
}
