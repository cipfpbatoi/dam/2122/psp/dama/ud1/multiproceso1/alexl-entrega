import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio3 {
    public static void main(String[] args) {
        //nos almacena en la veriable el nombre del usuario
        String nombre=System.getProperty("user.name");
        //ponemos la ruta donde se encuentra nuestro .jar
        String comando="java -jar C:\\Users\\"+nombre+"\\Desktop\\ejercicioarturo\\Ejercicio3\\out\\artifacts\\Ejercicio3_jar\\Ejercicio3.jar";
        List<String> lista= new ArrayList<>(Arrays.asList(comando.split(" ")));

        try {
            //creamos el proceso y lo ejecutamos
            Process process=new ProcessBuilder(lista).start();
            OutputStream os = process.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);
            //Creamos el scaner
            Scanner teclado = new Scanner(System.in);
//escribimos en linea en lo que se introduzca pasado a minusculas
            String line= teclado.nextLine().toLowerCase();
            //mientras que la linea no sea igual a finalizar no acaba
            while (!line.equals("finalizar")){
                //escribe en el buffer la linea
                bw.write(line);
                //hace el salto de linea
                bw.newLine();
                //hacemos el flush
                bw.flush();
                // lo mostramos en pantalla
                System.out.println(line);
                //escribimos en linea en lo que se introduzca pasado a minusculas
                line=teclado.nextLine().toLowerCase();

            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
